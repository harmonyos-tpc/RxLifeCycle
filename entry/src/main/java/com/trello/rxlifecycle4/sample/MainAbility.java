/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.sample;

import ohos.aafwk.content.Intent;

import com.trello.rxlifecycle4.components.RxAbility;
import com.trello.rxlifecycle4.sample.slice.MainAbilitySlice;

/**
 * This is MainAbility class which will run MainAbilitySlice
 */
public class MainAbility extends RxAbility {
    private static final String TAG_LOG = MainAbility.class.getName();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        LogUtil.info(TAG_LOG, "onStart()");
    }

    @Override
    protected void onActive() {
        super.onActive();
        LogUtil.info(TAG_LOG, "onActive()");
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        LogUtil.info(TAG_LOG, "onInactive()");
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        LogUtil.info(TAG_LOG, "onForeground()");
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        LogUtil.info(TAG_LOG, "onBackground()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.info(TAG_LOG, "onStop()");
    }
}
