# RxLifecycle

This library allows us to automatically complete sequences based on a second lifecycle stream.
This capability is useful in openharmony, where incomplete subscriptions can cause memory leaks.
# RxLifecycle includes :
* Bind with openharmony lifecycle event.
* Support for the Ability Lifecycle event.
* Support for the AbilitySlice Lifecycle event.
* Support for the Fraction Lifecycle event.
* Support for the FractionAbility Lifecycle event.

# Usage Instructions
You must start with an Observable<T> representing a lifecycle stream. Then you use RxLifecycle to bind a sequence to that lifecycle.
## Bind with the lifecycle event:
```
public class MainAbility extends Ability {
    private static final String TAG_LOG = MainAbility.class.getName();
    LifecycleProvider provider = HarmonyLifecycle.createLifecycleProvider(this);
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG, "Unsubscribed the event started in onStart()"))
                .compose(provider.bindToLifecycle())
                .subscribe(num -> LogUtil.info(TAG_LOG,
                        "Started in onStart(), running until in onStop(): " + num));
    }
}
```
  
## Bind with a specific lifecyle event occurs:
```
public class MainAbility extends Ability {
    private static final String TAG_LOG = MainAbility.class.getName();
    LifecycleProvider provider = HarmonyLifecycle.createLifecycleProvider(this);
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG, "Unsubscribed the event started onStart()"))
                .compose(provider.bindUntilEvent(AbilityEvent.STOP))
                .subscribe(num -> LogUtil.info(TAG_LOG,
                        "Started in onStart(), running until in onStop(): " + num));
    }
}
```
  
## Bind with the Ability lifecycle event:
Use rxlifecycle_components and subclass the provided RxAbility, RxAbilityFraction, etc. classes.
If you use rxlifecycle-components, just extend the appropriate class, then use the built-in bindToLifecycle() (or bindUntilEvent()) methods:
```java
public class MainAbility extends RxAbility {
    private static final String TAG_LOG = MainAbility.class.getName();
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG, "Unsubscribed the event started onStart()"))
                .compose(bindUntilEvent(AbilityEvent.STOP))
                .subscribe(num -> LogUtil.info(TAG_LOG,
                        "Started in onStart(), running until in onStop(): " + num));
    }
}
```
## Bind with the Fraction lifecycle event:
```
public class TestFraction extends RxFraction {
    private static final String TAG_LOG = MainAbility.class.getName();
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info(TAG_LOG, "Unsubscribed the event started onStart()"))
                .compose(bindUntilEvent(AbilityEvent.STOP))
                .subscribe(num -> LogUtil.info(TAG_LOG,
                        "Started in onStart(), running until in onStop(): " + num));
    }
}
```

# Installation tutorial

# Library Dependencies
RxLifecycle is dependent on Rxohos and rxjava3.

1.	For using RxLifecycle modules in your sample application, add below dependencies to generate hap/har:

        Modify entry build.gradle as below :
        ```
        dependencies {
           implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
           implementation 'io.reactivex.rxjava3:rxjava:3.0.4'
           compile project(path: ':rxlifecycle')
           compile project(path: ':rxlifecycle_harmony_lifecycle')
           compile project(path: ':rxlifecycle_harmony')
           compile project(path: ':rxlifecycle_components')
        }
        ```
2. For using RxLifecycle in separate application, add the below dependencies and include  "rxlifecycle.har", "rxlifecycle_harmony.har", "rxlifecycle_harmony_lifecycle.har" and "rxlifecycle_components.har" in libs folder of "entry" module  :

      Modify entry build.gradle as below :
        ```
        dependencies {
            implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
            implementation 'io.reactivex.rxjava3:rxjava:3.0.4'
        }
        ```
3. For using RxLifecycle from a remote repository in separate application, add the below dependencies "entry" module  :

    Modify entry build.gradle as below :
    ```
    dependencies {
    	implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
    	implementation 'io.openharmony.tpc.thirdlib:RxLifecycle_Components:1.0.1'
        implementation 'io.reactivex.rxjava3:rxjava:3.0.4'
       }
    ```
  
# License

Copyright (C) 2016 Trello

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License
