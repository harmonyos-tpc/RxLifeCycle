/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.harmony;

/**
 * Lifecycle events that can be emitted by Fraction.
 */
public enum FractionEvent {
    /**
     * Attach of event
     */
    ATTACH,
    /**
     * Start of event
     */
    START,
    /**
     * Foreground of event
     */
    FOREGROUND,
    /**
     * Active event value
     */
    ACTIVE,
    /**
     * Inactive event value
     */
    INACTIVE,
    /**
     * Background event value
     */
    BACKGROUND,
    /**
     * Stop of event
     */
    STOP,
    /**
     * Detach of event
     */
    DETACH
}
