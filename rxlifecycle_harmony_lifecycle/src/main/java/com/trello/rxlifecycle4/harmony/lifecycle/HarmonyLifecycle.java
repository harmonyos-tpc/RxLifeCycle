/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.harmony.lifecycle;

import ohos.aafwk.ability.ILifecycle;
import ohos.aafwk.ability.Lifecycle;
import ohos.aafwk.ability.LifecycleObserver;
import ohos.aafwk.content.Intent;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.BehaviorSubject;

import com.trello.rxlifecycle4.LifecycleProvider;
import com.trello.rxlifecycle4.LifecycleTransformer;
import com.trello.rxlifecycle4.RxLifecycle;
import com.trello.rxlifecycle4.rxlifecycle_annotation.CallSuper;
import com.trello.rxlifecycle4.rxlifecycle_annotation.CheckResult;
import com.trello.rxlifecycle4.rxlifecycle_annotation.NonNull;


/**
 * Wraps {@link ILifecycle} so that it can be used as {@link LifecycleProvider}. For example,
 * you can do
 * <pre>{@code
 * LifecycleProvider<Lifecycle.Event> provider = HarmonyLifecycle.createLifecycleProvider(this);
 * myObservable
 * .compose(provider.bindLifecycle())
 * .subscribe();
 * }</pre>
 * where {@code this} is {@code ohos.aafwk.ability.LifecycleObserver}
 *
 * @since 2020-12-22
 */
public final class HarmonyLifecycle extends LifecycleObserver implements LifecycleProvider<Lifecycle.Event> {
    private final ILifecycle owner;

    private final BehaviorSubject<Lifecycle.Event> lifecycleSubject = BehaviorSubject.create();

    private HarmonyLifecycle(ILifecycle owner) {
        this.owner = owner;
        owner.getLifecycle().addObserver(this);
    }

    /**
     * Provider for Lifecycle event
     *
     * @param owner lifecycle interface
     * @return lifecycle provider
     */
    public static LifecycleProvider<Lifecycle.Event> createLifecycleProvider(ILifecycle owner) {
        return new HarmonyLifecycle(owner);
    }

    @NonNull
    @Override
    @CheckResult
    public Observable<Lifecycle.Event> lifecycle() {
        return lifecycleSubject.hide();
    }

    @NonNull
    @Override
    @CheckResult
    public <T> LifecycleTransformer<T> bindUntilEvent(@NonNull Lifecycle.Event event) {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event);
    }

    @NonNull
    @Override
    @CheckResult
    public <T> LifecycleTransformer<T> bindToLifecycle() {
        return RxLifecycleHarmonyLifecycle.bindLifecycle(lifecycleSubject);
    }

    @Override
    @CallSuper
    public void onStart(Intent intent) {
        super.onStart(intent);
        lifecycleSubject.onNext(Lifecycle.Event.ON_START);
    }

    @Override
    @CallSuper
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        lifecycleSubject.onNext(Lifecycle.Event.ON_FOREGROUND);
    }

    @Override
    @CallSuper
    public void onActive() {
        super.onActive();
        lifecycleSubject.onNext(Lifecycle.Event.ON_ACTIVE);
    }

    @Override
    @CallSuper
    public void onInactive() {
        lifecycleSubject.onNext(Lifecycle.Event.ON_INACTIVE);
        super.onInactive();
    }

    @Override
    @CallSuper
    public void onBackground() {
        lifecycleSubject.onNext(Lifecycle.Event.ON_BACKGROUND);
        super.onBackground();
    }

    @Override
    @CallSuper
    public void onStop() {
        lifecycleSubject.onNext(Lifecycle.Event.ON_STOP);
        owner.getLifecycle().removeObserver(this);
        super.onStop();
    }
}
